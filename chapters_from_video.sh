#!/bin/bash

# Check whether exiftool is available and error out if not.
if ! which exiftool >/dev/null; then
  echo "Please install exiftool."
  if [[ "$OSTYPE" == "linux-gnu" ]]; then
    echo "On Ubuntu / Debian-based distros, 'apt-get install exiftool' should work."
  elif [[ "$OSTYPE" == "msys" ]]; then
    echo "On Windows, download exiftool from https://oliverbetz.de/pages/Artikel/ExifTool-for-Windows"
    echo "and install it. Then open a new bash window."
  fi
  exit 1
fi

# Check whether video exists, generate filename of output file
if [ -f "${1}" ]; then
  videofile="${1}"
  chapterlistfile="${videofile%.*}_chapters.txt"
else
  echo "Error: ${1} does not exist. Aborting."
  exit 2
fi

# Check whether chapter list file exists. Abort if it does
# to avoid messing something up.
if [ -f "${chapterlistfile}" ]; then
  echo "Error: ${chapterlistfile} already exists. Aborting."
  exit 3
fi

# Create exiftool config file to enable LargeFileSupport,
# since videos are frequently larger than 4 GB.
configfile=$(mktemp)
echo '%Image::ExifTool::UserDefined::Options = (
    LargeFileSupport => 1,
);' > "${configfile}"


# Run exiftool and then turn the "Chapter List" line into a format
# that Youtube recognizes as chapter list when pasted into the description.
exiftool -config "${configfile}" "${videofile}" | \
  sed -n -e '/^Chapter List[ ]\+: /p' | \
  sed -e 's|^Chapter List[ ]\+: ||' \
      -e 's|[,]\?[ ]\?\[\([0-9]:[0-9][0-9]:[0-9][0-9]\)\.[0-9]\{3\}\]|\n\1|g' | \
  sed -e '/^$/d' > "${chapterlistfile}"
rm -f "${configfile}"

# Check whether we've written more than 0 bytes. If not, clean up the
# empty file and error out.
if ! [ -s "${chapterlistfile}" ]; then
  echo "Exiftool did not return a chapter list."
  rm "${chapterlistfile}"
  exit 4
else
  echo "Successfully wrote chapter list to ${chapterlistfile}."
fi
