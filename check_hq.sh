#!/bin/bash

if [[ "$1" == "" ]]; then
  echo "Syntax: $0 <youtube url> [<e-mail address for notification>]"
  echo
  echo "This script continuously checks whether a given Youtube video is"
  echo "available in high quality (4k, VP9 codec)."
  echo "If you provide an e-mail address as second argument, a mail will"
  echo "be sent to that address, once the video becomes available in high"
  echo "quality. This requires a working 'mail' command for sending mail"
  echo "from this computer."
  exit 1
fi

LQ_FORMAT='137'
HQ_FORMAT='(308|313|315)'

RETRIES=3
WAIT_TIME=300

# Check which tool is available
youtubedl_available=0
ytdlp_available=0
youtubedl_invocation="youtube-dl"
ytdlp_invocation="yt-dlp"
which ${youtubedl_invocation} &>/dev/null && youtubedl_available=1
which ${ytdlp_invocation} &>/dev/null && ytdlp_available=1

# Error out if none is available
if (( ! ${youtubedl_available} && ! ${ytdlp_available} )); then
  echo "Error: Please install yt-dlp (preferred) or youtube-dl."
  exit 3
elif (( ${ytdlp_available} )); then
  # Prefer yt-dlp as youtube-dl doesn't seem to work right now...
  tool="${ytdlp_invocation}"
else
  tool="${youtubedl_invocation}"
fi

result_table="$(${tool} -F $1)"
if [ ! $? == 0 ]; then
  if ! echo "${result_table}" | grep -q -E "^${LQ_FORMAT}"; then
    echo "Error: Video is not even available in low quality - maybe wrong URL?"
    echo "       If you are sure the URL is right and the video is not set to"
    echo "       private, try updating ${tool}."
    echo "       On Ubuntu, use: sudo pip3 install -U ${tool}"
    exit 2
  fi
elif echo "${result_table}" | grep -q -E "^${HQ_FORMAT}"; then
  echo "High quality version (format ${HQ_FORMAT}) is already available:"
  echo
  echo "${result_table}"
  exit 0
fi

echo "Starting to monitor $1 for high quality format ${HQ_FORMAT}..."
while true; do
  # Wait for WAIT_TIME (randomly shortened by up to 60 seconds)
  wait=$(( ${WAIT_TIME} - ${RANDOM} % 60 ))
  echo -ne "- [ Waiting ${wait}s... ]"
  sleep ${wait}
  echo "  Checking at $(date)"
  error=0
  # Get result table. If that fails, retry for RETRIES times.
  if ! result_table="$(${tool} -F $1)"; then
    while [[ ${error} -le ${RETRIES} ]]; do
      if [[ ${error} -lt ${RETRIES} ]]; then
        sleep ${wait}
        echo "Check failed at $(date). Retrying ($(( ${error} + 1 ))/${RETRIES})."
      fi
      error=$(( ${error} + 1 ))
    done
  fi
  if [[ ${error} -gt 0 ]]; then
    echo "${tool} reported an error. Aborting."
    if [[ "$2" != "" ]]; then
      echo "Sending error notification e-mail to $2..."
      echo -e "Checking aborted at $(date) because ${tool} reported an error." | mail -s "Error - HQ video check aborted for $1" "$2"
    fi
    exit 1
  fi
  if echo "${result_table}" | grep -q -E "^${HQ_FORMAT}"; then
    break
  fi
done

echo "High quality version (format ${HQ_FORMAT}) is now available:"
echo
echo "${result_table}"

if [[ "$2" != "" ]]; then
  echo "Sending notification e-mail to $2..."
  echo -e "Youtube video $1 is now available in high quality (format ${HQ_FORMAT}):\n\n${result_table}" | mail -s "HQ video available for $1" "$2"
fi
