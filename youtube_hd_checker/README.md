# youtube-hd-checker discord bot

## Introduction

This is my first attempt at a discord bot.

Its purpose is to repeatedly check a newly-uploaded Youtube video and tell the requester once the video is available in high quality - i.e. compression with the better VP9 or AV1 codec has happened. Since this can easily take >24 hours for small Youtube channels, it is quite convenient to let the bot take over this task.

## Architecture / design

The bot consists of a frontend (`frontend.py`) - that listens for triggers and places jobs in the queue, and a backend (`checker.py`) that does the actual checking, as well as notification of the requester when a check succeeded, timed out or exceeded the max. retry limit on error.

The job queue is implemented as (MariaDB/MySQL) database (see `db.sql` for the schema).

## Current limitations

- As I am not expecting lots of concurrent requests from the small community I'm planning to use this bot in, I lazily implemented just a global queue. That means with every concurrent request, the time between checks increases.
- In order to be a friendly Youtube user (and not get the bot's IP address blacklisted), the bot waits between every request. Additionally it also waits between job queue polls from the database.

## Environment variables

Both `frontend.py` and `checker.py` expect the following environment variables to be set:

| Env. variable | Description                                                 |
| ------------- | ----------------------------------------------------------- |
| TOKEN         | Discord token to allow the bot to connect                   |
| DATABASE      | Name of the MariaDB/MySQL database that holds the job queue |
| DB_USER       | Database user                                               |
| DB_PASSWORD   | Database user's password                                    |

## Setup

1. Set up a Discord bot account and retrieve the token
1. Set up a local MariaDB/MySQL database and use 'db.sql' to create the `requests` table.
1. Install the required Python modules (`pip3 -r requirements.txt`)
1. Create two wrapper scripts that ...
    1. both export all necessary environment variables (see above)
    1. one launches `checker.py` 
    1. the other one launches `frontend.py`
1. Use screen or tmux to run the two wrapper scripts (and keep them running)

The scripts will print various messages to the console during operation, so that you know what's going on and who is using the bot. It does not log anything to a file.
