import discord
import os
import time
from common import yt, db, log

# Wait x seconds between checks
wait_time = 30

# Wait x seconds between database polls
db_poll_wait = 120

# Maximum number of hours to check a video
timeout = 72

# Maximum number of retries
retries = 3

intents = discord.Intents.default()
intents.message_content = True

client = discord.AutoShardedClient(intents=intents)

@client.event
async def on_ready():
    log.to_console('Logged in as {0.user}'.format(client))
    while(True):
        log.to_console('Getting jobs from database...')
        jobs = db.get_active_jobs()
        if jobs:
            log.to_console('Retrieved ' + str(len(jobs)) + ' job(s) from the database.')
            for job in jobs:
                # job[0]: discord_user_id
                # job[1]: url
                # job[2]: check_count
                # job[3]: check_start_date
                # job[4]: check_time (in hours)
                log.to_console('[' + job[1] + '] Getting info...')
                info, check_later = yt.get_info_from_url(job[1])
                if check_later:
                    log.to_console('[' + job[1] + '] Nope... will check back later...')
                    continue
                if not info:
                    log.to_console('[' + job[1] + '] Something is wrong with this one. Incrementing retry count.')
                    db.increment_retry_count(job[0], job[1])
                    continue
                log.to_console('[' + job[1] + '] Incrementing check_count...')
                db.increment_check_count(job[0], job[1])
                formats = info.get('formats')
                for format in formats:
                    id = format.get('format_id')
                    note = format.get('format_note')
                    if id in yt.hqformat_id_candidates and note != 'DASH video':
                        requester = await client.fetch_user(job[0])
                        log.to_console('[' + job[1] + '] Notifying user ' + str(requester.name) + ' about completion...')
                        await requester.send('Your video (' + job[1] + ') is now available in high quality! 🥳\nI\'ve checked it ' + str(job[2]) + ' times since ' + str(job[3]) + ' (' + str(job[4]) + ' hours).')
                        log.to_console('[' + job[1] + '] Marking job as succeeded...')
                        db.mark_job_as_succeeded(job[0], job[1])
                        break
                log.to_console('Waiting ' + str(wait_time) + 's...')
                time.sleep(wait_time)
        log.to_console('Checking for timed out jobs...')
        timed_out_jobs = db.check_and_mark_jobs_as_timed_out(timeout)
        if timed_out_jobs:
            log.to_console(str(len(timed_out_jobs)) + ' timed out job(s) deactivated.')
            for job in timed_out_jobs:
                requester = await client.fetch_user(job[0])
                log.to_console('Notifying user about timeout...')
                await requester.send('I\'m sorry, even after checking your video (' + job[1] + ') for ' + str(timeout) + ' hours, there is still no high-quality version. I\'ve stopped checking now.')
        log.to_console('Checking for max. retried jobs...')
        max_retried_jobs = db.check_and_remove_max_retried_jobs(retries)
        if max_retried_jobs:
            log.to_console(str(len(max_retried_jobs)) + ' max. retried job(s) deactivated.')
            for job in max_retried_jobs:
                requester = await client.fetch_user(job[0])
                log.to_console('Notifying user about max. retries...')
                await requester.send('I\'m sorry, there seems to be something wrong with your video (' + job[1] + '). I\'ve got an error from Youtube and retried ' + str(retries) + ' times, without success. I\'ve stopped checking now.')
        log.to_console('Waiting ' + str(db_poll_wait) + 's...')
        time.sleep(db_poll_wait)

client.run(os.getenv('TOKEN'))