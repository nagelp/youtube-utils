import os
import time
import MySQLdb
import datetime
import pprint

# Expected environment variables:
# - TOKEN       discord token
# - DATABASE    name of MySQL database to connect to for storing / retrieving check jobs
# - DB_USER     database user
# - DB_PASSWORD database password

def get_active_jobs():
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("SELECT `discord_user_id`, `url`, `check_count`, `check_start_date`, TIMESTAMPDIFF(HOUR, `check_start_date`, NOW()) as `check_time` \
               FROM `requests` WHERE (`check_active`=1)")
    query_result = c.fetchall()
    #pp = pprint.PrettyPrinter()
    #pp.pprint(query_result)
    c.close()
    db.close()
    if (len(query_result) > 0):
        return query_result
    else:
        return False

def get_status_str(discord_user_id):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("SELECT url, check_start_date, check_count FROM `requests` WHERE (`check_active`='1' AND `discord_user_id`=%s) ORDER BY `check_start_date`", (discord_user_id, ))
    query_result = c.fetchall()
    c.close()
    db.close()
    if (len(query_result) > 0):
        status_str = 'Here is your status (active requests):\n```\n'
        for row in query_result:
            status_str += 'Video: ' + str(row[0]) + ' - ' + 'started checking on ' + str(row[1]) + ', checked ' + str(row[2]) + ' times so far.\n'
        status_str += '```'
        return status_str
    else:
        return 'Looks like there are no active checks requested by you.'

def get_status_done_str(discord_user_id):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("SELECT url, check_start_date, check_end_date, check_count FROM `requests` WHERE (`check_active`='0' AND `discord_user_id`=%s) ORDER BY `check_start_date`", (discord_user_id, ))
    query_result = c.fetchall()
    c.close()
    db.close()
    if (len(query_result) > 0):
        status_str = 'Here is your status (done requests):\n'
        for row in query_result:
            try:
                duration = str(row[2] - row[1])
            except:
                duration = 'n/a'
            status_str += '[' + str(row[1]) + '] ' + 'Video: ' + str(row[0]) + ' - ' + 'Total check duration: ' + duration + ', checked ' + str(row[3]) + ' times.\n'
        return status_str
    else:
        return 'Looks like there are no done requests from you (or they were purged from my database).'

def increment_check_count(discord_user_id, check_url):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("UPDATE `requests` SET `check_count` = `check_count` + 1, `retry_count` = 0 WHERE `discord_user_id` = %s AND `url` = %s",
               (discord_user_id, check_url))
    c.close()
    db.commit()
    db.close()

def increment_retry_count(discord_user_id, check_url):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("UPDATE `requests` SET `retry_count` = `retry_count` + 1 WHERE `discord_user_id` = %s AND `url` = %s",
               (discord_user_id, check_url))
    c.close()
    db.commit()
    db.close()

def add_job(discord_user_id, check_url):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("INSERT INTO `requests` (`discord_user_id`, `url`, `check_active`) \
               VALUES (%s, %s, %s)",
               (discord_user_id, check_url, '1'))
    c.close()
    db.commit()
    db.close()

def mark_job_as_succeeded(discord_user_id, check_url):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    current_datetime = datetime.datetime.now()
    c.execute("UPDATE `requests` SET `check_active` = '0', `check_end_date` = %s, `check_success` = '1' WHERE `discord_user_id` = %s AND `url` = %s",
               (current_datetime, discord_user_id, check_url))
    c.close()
    db.commit()
    db.close()

def check_and_mark_jobs_as_timed_out(timeout):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("SELECT `discord_user_id`, `url` FROM `requests` WHERE `check_active` = 1 AND TIMESTAMPDIFF(HOUR, `check_start_date`, NOW()) > %s", (timeout, ))
    query_result = c.fetchall()
    c.execute("UPDATE `requests` SET `check_success` = '0', `check_active` = '0' WHERE `check_active` = 1 AND TIMESTAMPDIFF(HOUR, `check_start_date`, NOW()) > %s", (timeout, ))
    c.close()
    db.commit()
    db.close()
    if (len(query_result) > 0):
        return query_result
    else:
        return False

def check_and_remove_max_retried_jobs(retries):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("SELECT `discord_user_id`, `url` FROM `requests` WHERE `check_active` = 1 AND `retry_count` > %s", (retries, ))
    query_result = c.fetchall()
    c.execute("UPDATE `requests` SET `check_success` = '0', `check_active` = '0' WHERE `check_active` = 1 AND `retry_count` > %s", (retries, ))
    c.close()
    db.commit()
    db.close()
    if (len(query_result) > 0):
        return query_result
    else:
        return False

def get_check_start_date_str(discord_user_id, check_url):
    db = MySQLdb.connect(user=os.getenv('DB_USER'), passwd=os.getenv('DB_PASSWORD'), db=os.getenv('DATABASE'))
    c = db.cursor()
    c.execute("SELECT check_start_date FROM `requests` \
               WHERE (`discord_user_id`=%s AND `url`=%s)",
               (discord_user_id, check_url))
    #print('Query: ' + str(c._executed))
    query_result = c.fetchone()[0]
    c.close()
    db.close()
    formatstring = '%Y-%m-%d %H:%M:%S ' + time.localtime().tm_zone
    return query_result.strftime(formatstring)