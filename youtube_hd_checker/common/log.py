from datetime import datetime

def to_console(text):
    now = datetime.now()
    print(now.isoformat(timespec='seconds') + "\t" + text)
