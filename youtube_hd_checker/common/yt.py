import yt_dlp
import pickle
from strip_ansi import strip_ansi

# From https://gist.github.com/AgentOak/34d47c65b1d28829bb17c24c04a0096f
hqformat_id_candidates = ['701', '700', '699', '402', '571', '401', '400', '399', '337', '336', '335', '272', '315', '308', '303', '313', '271', '248' ]

def get_info_from_url(url, testmode=False):
    info = None
    if not testmode:
        check_url = str(url)
        ydl_opts = { 'noplaylist': True, 'quiet': True, }
        ydl = yt_dlp.YoutubeDL(ydl_opts)
        with ydl:
            try:
                info = ydl.extract_info(check_url, download=False)
            except yt_dlp.utils.UnsupportedError as e:
                print('The URL you provided doesn\'t seem to be a valid Youtube URL.' + strip_ansi(str(e)))
                if ('Check back later' in strip_ansi(str(e))):
                    return None, True
                else:
                    return None, False
            except yt_dlp.utils.DownloadError as e:
                print('The URL you provided doesn\'t seem to be a valid Youtube URL: ' + strip_ansi(str(e)))
                if ('Check back later' in strip_ansi(str(e))):
                    return None, True
                else:
                    return None, False
        if info:
            # Uncomment the two lines below to write out a result.pickle file with every real run,
            # which can then be reused for test runs (testmode=True)
            #with open('result.pickle', 'wb') as f:
            #    pickle.dump(info, f)
            return info, False
    else:
        with open('result.pickle', 'rb') as f:
            info = pickle.load(f)
            return info, False

def get_string_from_info(info):
    title_msg = 'Video title: ' + info.get('title')
    formats_msg = 'Formats:\n`ID - Resolution - FPS - Video codec`\n'
    for format in info.get('formats'):
        formats_msg += '`' + format.get('format') + ' - ' + str(format.get('fps')) + ' - ' + format.get('vcodec') + '`\n'
    return(title_msg + '\n' + formats_msg)