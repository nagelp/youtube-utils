import discord
import os
import validators
import sys
import pprint
import MySQLdb
from common import yt, db, log

intents = discord.Intents.default()
intents.message_content = True

client = discord.AutoShardedClient(intents=intents)

@client.event
async def on_ready():
    log.to_console('Logged in as {0.user}'.format(client))
    log.to_console('Servers connected to:')
    for guild in client.guilds:
        log.to_console('- ' + str(guild.name))

@client.event
async def on_message(message):
    if message.author == client.user or message.author.bot:
        return

    # Handle the "trigger" messages
    if message.content.startswith('youtube-hd-checker') or client.user.mentioned_in(message):
        await message.author.send('Hi, ' + message.author.display_name + ', I am the youtube-hd-checker bot.\n_If there are any issues, please contact padde#9204._\n\nType \'help\' for more usage information.\n\nPlease paste the URL to your newly uploaded video, and I will let you know once the VP9/AV1-encoded HD / 4K version is ready.')
        log.to_console(message.author.name + " triggered the bot")

    # Handle messages someone sends directly to the bot
    if not message.guild:
        discord_user_id = str(message.author.id)
        if (str(message.content).lower() == 'help' or str(message.content).lower() == '!help'):
            log.to_console(message.author.name + ' requested help')
            helptext = 'If anyone writes my name \'youtube-hd-checker\' or mentions me, I will contact the sender of that message and introduce myself. From then on, everything else happens via direct messages (like this one).\n\nPlease paste the URL to your newly uploaded video, and I will let you know once the VP9/AV1-encoded HD / 4K version is ready.\n_Except for the special command words listed below, I will interpret everything you send to me as a Youtube video URL that I am supposed to check for you. If it turns out it is not a valid Youtube URL, I will let you know._\n\n**Commands**\n```\nhelp            get this text\nstatus          get the list of videos I am currently checking for you\nstatus done     get the list of videos I have previously checked for you\n```'
            await message.author.send(helptext)
        elif (str(message.content).lower() == 'status' or str(message.content).lower() == '!status'):
            log.to_console(message.author.name + ' requested status')
            status = db.get_status_str(discord_user_id)
            await message.author.send(status)
        elif (str(message.content).lower() == 'status done' or str(message.content).lower() == '!status done'):
            log.to_console(message.author.name + ' requested status done')
            status_longtext = db.get_status_done_str(discord_user_id)
            # Since this might get over the 2000 character limit of discord,
            # split it into chunks of 10 lines.
            status_text_lines = status_longtext.splitlines(True)
            status_text_chunk = []
            for line in status_text_lines:
                status_text_chunk.append(line)
                if len(status_text_chunk) >= 10:
                    await message.author.send('\n```\n' + '\n'.join(status_text_chunk) + '\n```')
                    status_text_chunk = []
            if len(status_text_chunk) > 0:
                await message.author.send('\n```\n' + '\n'.join(status_text_chunk) + '\n```')
        elif ((validators.url(message.content) and ('youtu.be' in message.content or 'youtube.com' in message.content)) or message.content.startswith('__test')):
            if message.content.startswith('__test'):
                log.to_console(message.author.name + ' requested testmode')
                await message.author.send('Testmode - getting last video\'s info from file...')
                info = yt.get_info_from_url(message.content, True)
            elif '&list=' in str(message.content).lower():
                log.to_console(message.author.name + ' requested playlist')
                await message.author.send('This appears to be a playlist URL. Only single video URLs can be checked.')
                return
            else:
                log.to_console(message.author.name + ' requested valid URL ' + message.content)
                await message.author.send('Looks like a Youtube URL! Getting more info... (this might take a few seconds).')
                info, check_later = yt.get_info_from_url(message.content)
            if info and not info.get('_type') == 'playlist':
                # Get the URL as Youtube normalizes it
                check_url = info.get('webpage_url')
                # Get list of format IDs
                formats = info.get('formats')
                for format in formats:
                    id = format.get('format_id')
                    note = format.get('format_note')
                    if id in yt.hqformat_id_candidates and note != 'DASH video':
                        log.to_console(message.author.name + ' requested video that is already available in high quality')
                        await message.author.send('High quality version is already available. No need to wait :) Here is a list of all available formats:')
                        msg = yt.get_string_from_info(info)
                        await message.author.send(msg)
                        return
            elif check_later:
                log.to_console('Got a "check back later" from Youtube for ' + message.content + ' - adding to database anyway.')
                # Assume URL is correct, because we got 'check back later', but we cannot get any additional info
                check_url = message.content
            else:
                log.to_console(message.author.name + ' requested URL that did not work out - ' + message.content)
                await message.author.send('I could not access the video. Make sure that:\n- you copy&pasted the URL correctly\n- the video is not set to private (must be unlisted or public for me to check it)\n- the video is available in Germany (where this bot is located)')
                return
            log.to_console('[' + check_url + '] Adding entry to database for ' + message.author.name)
            try:
                db.add_job(discord_user_id, check_url)
            except MySQLdb._exceptions.IntegrityError as e:
                log.to_console("Error %d: %s" % (e.args[0], e.args[1]))
                if(e.args[0] == 1062):
                    check_start_date = db.get_check_start_date_str(discord_user_id, check_url)
                    log.to_console(message.author.name + ' requested URL that he had requested before - ' + message.content)
                    await message.author.send('Duplicate request. Checking this video was requested by you on ' + check_start_date + '.')
                return
            except:
                log.to_console(message.author.name + ' triggered unknown database error.')
                await message.author.send('Sorry, there is something wrong with the database.')
                return
            await message.author.send('Looks good! I will now check this video every few minutes and let you know once the high-quality version is available.\nYou can also type \'status\' and I will let you know which videos I am checking for you at the moment.')
        else:
            log.to_console(message.author.name + ' requested an invalid URL - ' + message.content)
            await message.author.send('That does not look like a valid Youtube URL.')

client.run(os.getenv('TOKEN'))
