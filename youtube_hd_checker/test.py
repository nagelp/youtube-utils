from common import yt

info, check_later = yt.get_info_from_url('https://www.youtube.com/watch?v=2OiHfvmNkyA')
formats = info.get('formats')
format_ids = []
format_notes = []
for format in formats:
    id = format.get('format_id')
    note = format.get('format_note')
    print(id, note)
    if id in yt.hqformat_id_candidates and note != 'DASH video':
        print(id, note + ' = hq')
